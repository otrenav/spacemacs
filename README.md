
| [Website](https://links.otrenav.com/website) | [Twitter](https://links.otrenav.com/twitter) | [LinkedIn](https://links.otrenav.com/linkedin)  | [GitHub](https://links.otrenav.com/github) | [GitLab](https://links.otrenav.com/gitlab) | [CodeMentor](https://links.otrenav.com/codementor) |

# Spacemacs Configuration

- Omar Trejo
- July, 2017

This is my Spacemacs configuration. Feel free to use/adapt it. If you're going
to use my configuration, the `bookmarks` file is probably not useful for you, so
fix that, or delete it and adapt the `install.sh` accordingly. The rest of the
setup can be followed in the `install.sh` script.

If you don't know what it is, you should definitely check out the Spacemacs
project! It's a great way to enhance your Emacs experience. You can learn more
about it here: http://spacemacs.org/. To learn more about the GNU Emacs (the
best editor :P), go to: https://www.gnu.org/software/emacs/.

## Emacs notes

- `M-x describe-face`: used to find face underneath cursor
- `M-x list-faces-display`: show faces being used
- `load-file ENTER ENTER`: used to reload theme

---

> "The best ideas are common property."
>
> —Seneca
